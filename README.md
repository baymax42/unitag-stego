# Teory

UTF-8 encoding provides invisible characters which can be used for steganography purposes. This tool uses method in which unicode tags are involved.

It simply adds offset (at which tag characters appears) to given characters. Then embeds fragments of converted text near space characters. This operation can be reversed to decode message.
Works well for ASCII texts.

For reference go here: http://graphemica.com/blocks/tags

# Usage

Those two scripts are interactive, just simply launch them in Python 3.7.

# Changes

[v1.0] - Interactive encoding and decoding.
