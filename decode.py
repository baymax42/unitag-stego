# -*- coding: utf-8 -*-
# Unicode tags reference
# http://graphemica.com/blocks/tags

# All tags start with this 'offset'
TAG_OFFSET = ord('\U000e0000')


def decode(line):
    """
    Iterates through whole string and searches for unicode tags.
    If tags is found, offset is removed from it - converts to characters.
    :param line: line of unicode characters
    :return: string with decoded unicode tag characters
    """
    decoded = ''
    for char in line:
        if ord(char) > TAG_OFFSET:
            decoded += chr(ord(char) - TAG_OFFSET)
    return decoded


if __name__ == '__main__':
    content = ''
    path = input('Path: ')
    print('     > Tag encoded data <')
    with open(path, encoding='utf-8') as f:
        content = f.readlines()
        for line in content:
            data = decode(line)
            if len(data) > 0:
                print(data, )



