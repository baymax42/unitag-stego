# -*- coding: utf-8 -*-
# Unicode tags reference
# http://graphemica.com/blocks/tags
import re

# All tags start with this 'offset'
TAG_OFFSET = ord('\U000e0000')


def get_text(prompt):
    """
    Utility function for getting text from user with printing custom prompt message.
    :param prompt: Prompt message for user.
    :return: User text.
    """
    text = ''
    line = input(prompt + '\n')
    while len(line):
        text += line + '\n'
        line = input()

    return text


def get_segment(msg, index, length):
    """
    Utility function for getting fragment of text.
    :param msg: Text to be fragmented
    :param index: Index of segment to get
    :param length: Segment size
    :return:
    """
    return msg[index:index + length]


def encode(message):
    """
    Encode ASCII text with unicode tags characters
    :param message: Message to encode
    :return: Encoded message using unicode tags
    """
    encoded = ''
    for char in message:
        encoded += chr(TAG_OFFSET + ord(char))

    return encoded


def embed_message(text, message):
    """
    Covers ASCII message with text.
    It additionally segments message to embed in several places.
    :param text: Cover text
    :param message: ASCII message to embed
    :return: Text with embed message
    """
    data = text.rsplit(' ')
    segment_size = len(message) // len(data) + 1
    encoded_message = encode(message)

    covered_message = []
    current_segment = 0
    for word in data:
        # Ignore newlines to make it less detectable by user
        if not word.endswith('\n'):
            segment = get_segment(encoded_message, current_segment, segment_size)
            current_segment += segment_size
            word += segment
        covered_message.append(word)

    return ' '.join(covered_message)


if __name__ == '__main__':
    message = get_text('Secret message:')
    content = get_text('Cover text:')
    path = input('Where to save it? ')
    with open(path, encoding='utf-8', mode='x') as f:
        f.writelines(embed_message(content, message))
